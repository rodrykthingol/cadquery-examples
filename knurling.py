import cadquery as cq

r = 20

s1 = cq.Sketch().circle(r).edges().distribute(20).rect(1, 2).clean()
s2 = (
    cq.Sketch()
    .circle(r, mode="c", tag="c")
    .edges(tag="c")
    .distribute(20)
    .rect(1, 2)
    .clean()
)

w = cq.Workplane()

w1 = w.placeSketch(s1).twistExtrude(2 * r, 90)
w2 = w.placeSketch(s2).twistExtrude(2 * r, -90)

res = w1.union(w2, clean=True, tol=1e-2)

show_object(res)
