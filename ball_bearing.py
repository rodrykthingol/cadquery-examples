import cadquery as cq
from torus import torus

# Needs some organizing and add tolerances. Maybe rework cap

# Parameters
bb_radius = 26 # Outside radius of the ball bearing
bb_height = 12 # Height of the ball bearing
bb_gap_radius = 13 # radius where the gap between inner and outer sections of the ball bearing
bb_shaft_radius = 5 # radius of the shaft of the ball bearing
bb_ring_thickness = 2 
bb_cap_thickness = 2
tolerance = 0.25 # Fabricating process minimum tolerance

r = 2.35 # Radius of the bearing spheres
n = 12 # Number of bearing spheres

# Create outer section of the ball bearing
outer = cq.Workplane("XY").circle(bb_radius).extrude(bb_height).faces(">Z").hole(2*bb_gap_radius+bb_ring_thickness+2*tolerance)

# Create inner section of the ball bearing
inner = cq.Workplane("XY").circle(bb_gap_radius-bb_ring_thickness/2-tolerance).extrude(bb_height).faces(">Z").hole(2*bb_shaft_radius+2*bb_cap_thickness)

# Create bearing groove
groove = torus(bb_gap_radius,r).translate((0,0,bb_height/2))

# Create array of bearing balls

balls = cq.Workplane("XY")

for angle in range(0,360,int(360/n)):
    ball = cq.Workplane("XY").sphere(r).translate((bb_gap_radius,0,bb_height/2)).rotate((0,0,0),(0,0,1),angle)
    balls.add(ball)

# Create bearing sphere cage
cage = cq.Workplane("XY").circle(bb_gap_radius+bb_ring_thickness/2).extrude(bb_height/2+r/2).faces(">Z").hole(2*bb_gap_radius-bb_ring_thickness).cut(balls)

# Create entrance for bearing balls to insert inside the cage
ball_entrance = cq.Workplane("XY").circle(r).extrude(bb_height/2).translate((bb_gap_radius,0,bb_height/2))

# Create main body
main = outer.union(inner).cut(ball_entrance).cut(groove,False)

# Create ball bearing, without cap
ball_bearing = main.add(cage)

# Create ball bearing cap
bb_cap = cq.Workplane("XY").circle(bb_radius).extrude(bb_cap_thickness).faces(">Z").circle(bb_shaft_radius+bb_cap_thickness).extrude(bb_height).faces(">Z").hole(2*bb_shaft_radius,bb_height+bb_cap_thickness)

# Render objects
show_object(ball_bearing)
show_object(bb_cap)

# Export files
cq.exporters.export(ball_bearing,"ball_bearing.stl")
cq.exporters.export(bb_cap,"ball_bearing_cap.stl")
