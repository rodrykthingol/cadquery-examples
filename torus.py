import cadquery as cq

"""
# Parameters
R = 10 # Big circle radius
r = 5 # Small circle radius
"""

# Create torus object
def torus(R,r):
    object = cq.Workplane("XY").center(R,0).circle(r).revolve(360,[-R,0,0],[-R,1,0]).rotateAboutCenter([1,0,0],90)
    return object

"""
# Render torus object
object = torus(R,r)
show_object(object)
"""
