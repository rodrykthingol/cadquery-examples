from math import sin, cos, pi
import cadquery as cq

# Units (mm)

# Parameters
pitch = 1
height = 10
radius = 2
isLefthand = False

# Design code
helix_wire = cq.Wire.makeHelix(pitch, height, radius, lefthand=isLefthand)
helix = cq.Workplane(obj=helix_wire)
# Render
show_object(helix)
