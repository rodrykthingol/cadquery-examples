import cadquery as cq

r = 0.42  # Radius of the helix
p = 0.4  # Pitch of the helix

#### CHANGE TO 2.5 and no cut
#### CHANGE TO 3.0 and it hangs
h = 3  # Height of the helix

# Helix
wire = cq.Wire.makeHelix(pitch=p, height=h, radius=r)
helix = cq.Workplane(obj=wire)

shaft = (
    cq.Workplane("XY")
    .circle(0.1)
    .extrude(0.5, taper=-30)
    .faces(">Z")
    .wires()
    .toPending()
    .extrude(2.0)
)
# show_object(shaft)


# Final result. A circle sweeped along a helix.
threads = (
    cq.Workplane("XZ")
    .center(r, -0.5)
    .polyline(((-0.15, 0.05), (0.0, 0.01), (0, 0.39), (-0.15, 0.35)))
    .close()
    .sweep(helix, isFrenet=True)
    .transformed((-90, 0, 0))
    .workplane(2.5)
    .split(False, True)
)
# show_object(threads)

result = shaft.cut(threads)

# Render
show_object(result)
