import cadquery as cq
from math import sin, cos, tan, pi, acos, atan, sqrt, degrees, radians

# Gear parameters

module = 3  # mm
pressure_angle = 20  # degrees
number_of_teeth = 10  # Maximum number of teeth = 74 - Clearange issue
gear_height = 3
is_internal_gear = False

# Variable formulas

m = module
z = number_of_teeth
a_0 = radians(20)  # pressure angle (radians)
x = 0  # profile shift coefficient
c = 0.167 * m  # clearance - Adjust clearance with higher number of teeth (7/6 or 5/4)
d_0 = m * z  # reference pitch circle diameter
p_0 = pi * d_0 / z  # circular pitch reference circle
d_a = m * (z + 2 * x + 2)  # addendum circle diameter
d_b = d_0 * cos(a_0)  # base circle diameter
p_b = pi * m * cos(a_0)  # circular pitch base circle

d_d = m * (z + 2 * x - 2) - 2 * c  # dedendum circle diameter

# Calculate the involute's curve polar angle: phi or inv alpha
def inv_a(d):
    a = acos((d_b / 2) / (d / 2))  # Auxiliar angle for calculus
    return tan(a) - a


# Calculate the involute's curve cartesian coordinates - returns tuple (x,y)
def inv_cartesian(d, angle_offset=0, is_opposite=False):
    if is_opposite == False:
        return (
            d / 2 * cos(inv_a(d) + angle_offset),
            d / 2 * sin(inv_a(d) + angle_offset),
        )
    else:
        return (
            d / 2 * cos(-inv_a(d) + angle_offset),
            d / 2 * sin(-inv_a(d) + angle_offset),
        )


# Calculate a circle's cartesian coordinates - return tuple (x,y)
def circle_cartesian(r, t):
    return (r * cos(t), r * sin(t))


# Calculate the arc length in radians of a gear's tooth thickness at the diameter of a given circle
def polar_tooth_thickness(d):

    # Tooth thickness at reference pitch circle
    s_0 = m * (pi / 2 + 2 * x * tan(a_0))

    # Tooth thickness
    s = d * (s_0 / d_0 + tan(a_0) - a_0 - inv_a(d))

    # Calculate circular pitch
    p = pi * d / z

    # Calculate circumference
    cf = z * p

    return 2 * pi * s / cf


# Calculate the arc length in radians of the chosen circular pitch
def polar_arc_length(p):
    return 2 * pi * p / (z * p)


# Create blank object
inv_gear = cq.Workplane("XY")

# Clearance modifications for internal gear
if is_internal_gear == True:
    d_a = d_a + 2 * c  # Increase addedum circle diameter
    d_d = (
        d_d + 2 * c
    )  # Increase dedendum circle diameter. Note that dedendum is on the ouside in a internal gear

# Create individual gear profiles
for n in range(z):

    # Object positional parameters
    phi_0 = 0 + n * polar_arc_length(p_b)
    phi_1 = phi_0 + inv_a(d_a)
    phi_2 = phi_1 + polar_tooth_thickness(d_a)
    phi_3 = phi_0 + polar_tooth_thickness(d_b)
    phi_4 = phi_0 + polar_arc_length(p_b)

    pts_right_flank = [
        (circle_cartesian(d_b / 2, phi_3 + polar_arc_length(p_b))),
        (circle_cartesian(d_d / 2, phi_3 + polar_arc_length(p_b))),
    ]
    pts_left_flank = [
        (circle_cartesian(d_b / 2, phi_4 + polar_arc_length(p_b))),
        (circle_cartesian(d_d / 2, phi_4 + polar_arc_length(p_b))),
    ]

    # Model object
    inv_gear = (
        inv_gear.parametricCurve(
            lambda d: inv_cartesian(d_b + d * (d_a - d_b), phi_0), makeWire=False
        )
        .toPending()
        .parametricCurve(
            lambda d: inv_cartesian(d_b + d * (d_a - d_b), phi_3, True), makeWire=False
        )
        .toPending()
        .parametricCurve(
            lambda t: circle_cartesian(d_a / 2, phi_1 + t * (phi_2 - phi_1)),
            makeWire=False,
        )
        .toPending()
        .parametricCurve(
            lambda t: circle_cartesian(d_d / 2, phi_3 + t * (phi_4 - phi_3)),
            makeWire=False,
        )
        .toPending()
        .spline(pts_right_flank)
        .spline(pts_left_flank)
        .consolidateWires()
    )

"""
# Visual help 2d objects
addendum_circle = cq.Workplane("XY").circle(d_a / 2)
pitch_ref_circle = cq.Workplane("XY").circle(d_0 / 2)
base_circle = cq.Workplane("XY").circle(d_b / 2)
dedendum_circle = cq.Workplane("XY").circle(d_d / 2)
"""

# Logic for creation of internal or external gear

if is_internal_gear == True:
    offset = 2
    r = d_a / 2 + offset / 2  # radius of internal gear
    inv_gear = inv_gear.circle(r).consolidateWires()
else:
    inv_gear = inv_gear.toPending().extrude(gear_height)


# Render object
show_object(inv_gear)

# Export
# cq.exporters.export(inv_gear, "inv_gear.stl")
# cq.exporters.export(inv_gear.section(), "inv_gear.dxf")
# cq.exporters.export(inv_gear, "inv_gear.step")
